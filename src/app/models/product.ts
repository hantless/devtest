
export class Product {

  id: string;
  name: string;
  manager?: string;
  startDate: string;

}
