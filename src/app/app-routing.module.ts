import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthenticatedComponent } from './authenticated/authenticated.component';
import { AuthGuard } from './guards/auth.guard';
import { WelcomeComponent } from './authenticated/welcome/welcome.component';
import { NewproductComponent } from './authenticated/newproduct/newproduct.component';
import { SalesComponent } from './authenticated/sales/sales.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    component: AuthenticatedComponent,
    canActivate: [
      AuthGuard
    ],
    children: [
      {
        path: '',
        component: WelcomeComponent,
        pathMatch: 'full'
      },
      {
        path: 'new',
        component: NewproductComponent
      },
      {
        path: 'sales',
        component: SalesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
