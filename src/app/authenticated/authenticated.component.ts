import { Component, OnInit } from '@angular/core';
import { SalesdataService } from '../service/salesdata.service';

@Component({
  selector: 'app-authenticated',
  templateUrl: './authenticated.component.html',
  styleUrls: ['./authenticated.component.scss']
})
export class AuthenticatedComponent implements OnInit {

  constructor(private salesService: SalesdataService) { }

  ngOnInit(): void {
  }

}
