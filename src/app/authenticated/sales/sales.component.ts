import { Component, OnInit } from '@angular/core';
import { ProductSales } from '../../models/productSales';
import { SalesdataService } from '../../service/salesdata.service';

@Component({
  selector: 'app-sales',
  templateUrl: './sales.component.html',
  styleUrls: ['./sales.component.scss']
})
export class SalesComponent implements OnInit {

  salesData: ProductSales[];
  cols: any[];

  constructor(
    private salesService: SalesdataService
  ) { }

  ngOnInit(): void {
    this.salesData = this.salesService.getSales();
    this.cols = this.salesService.getCols();
    console.log(this.salesData);
  }

  totalSales(sales) {
    return sales.salesQ1 + sales.salesQ2 + sales.salesQ3 + sales.salesQ4;
  }

}
