import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/product';
import { NgForm } from '@angular/forms';
import { SalesdataService } from '../../service/salesdata.service';

@Component({
  selector: 'app-newproduct',
  templateUrl: './newproduct.component.html',
  styleUrls: ['./newproduct.component.scss']
})
export class NewproductComponent implements OnInit {

  aProduct: Product = {
    id: null,
    name: null,
    manager: null,
    startDate: null
  };

  constructor(
    private salesService: SalesdataService
  ) { }

  ngOnInit(): void {
  }

  clearForm(form: NgForm): void {
    form.reset();
  }

  addProduct(form: NgForm): void {
    this.salesService.addProduct(form.value);
    form.reset();
  }
}
