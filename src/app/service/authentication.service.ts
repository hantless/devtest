import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  private loggedIn = false;

  constructor() { }

  login(username, password): boolean {

    /* mockup for an external validation service */
    if (username === 'devtest' && password === 'devtest') {
      this.loggedIn = true;
      return true;
    }
    this.loggedIn = false;
    return false;

  }

  isLogged(): boolean {
    return this.loggedIn;
  }

}
