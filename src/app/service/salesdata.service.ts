import { Injectable } from '@angular/core';
// @ts-ignore
import * as data from './potato_sales.json';
import { ProductSales } from '../models/productSales';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class SalesdataService {

  private salesData: ProductSales[];
  private cols;

  constructor() {
    console.log(data);
    this.salesData = data.data;
    this.cols = data.column;
  }

  getSales(): ProductSales[] {
    return this.salesData;
  }

  getCols() {
    return this.cols;
  }

  addProduct(product: Product): void {
    let productSales = new ProductSales();
    productSales.productID = product.id;
    productSales.productName = product.name;

    this.salesData.push(productSales);
  }

}
