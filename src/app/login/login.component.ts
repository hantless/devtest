import { Component, OnInit } from '@angular/core';
import { Loginfields } from '../models/loginfields';
import { NgForm } from '@angular/forms';
import { AuthenticationService } from '../service/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginError = false;

  loginfields: Loginfields = {
    username: null,
    password: null
  };

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm): void {
    if (this.authenticationService.login(form.value.username, form.value.password)) {
      this.router.navigate(['']);
      return;
    }

    this.loginError = true;
  }

}
